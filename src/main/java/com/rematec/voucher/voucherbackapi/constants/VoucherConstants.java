package com.rematec.voucher.voucherbackapi.constants;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;

@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class VoucherConstants {
}
