package com.rematec.voucher.voucherbackapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VoucherbackApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(VoucherbackApiApplication.class, args);
	}

}
