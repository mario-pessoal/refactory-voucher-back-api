package com.rematec.voucher.voucherbackapi.services;

import com.rematec.voucher.models.PerfilApiRequest;
import com.rematec.voucher.models.PerfilApiResponse;
import com.rematec.voucher.models.PerfilResumidoApiResponse;
import com.rematec.voucher.models.PerfilUpdateApiRequest;
import com.rematec.voucher.voucherbackapi.exceptios.EmpresaNaoEncontradaException;
import com.rematec.voucher.voucherbackapi.exceptios.NaoPermitidoExcluirPerfilException;
import com.rematec.voucher.voucherbackapi.exceptios.BadRequestException;
import com.rematec.voucher.voucherbackapi.exceptios.PerfilCadastradoException;
import com.rematec.voucher.voucherbackapi.exceptios.PerfilNaoEncontradoException;
import com.rematec.voucher.voucherbackapi.mapper.VouckBackMapper;
import com.rematec.voucher.voucherbackapi.models.entities.EmpresaEntity;
import com.rematec.voucher.voucherbackapi.repositories.IEmpresaRepository;
import com.rematec.voucher.voucherbackapi.repositories.IPerfilRepository;
import com.rematec.voucher.voucherbackapi.repositories.IUsuarioRepository;
import com.rematec.voucher.voucherbackapi.models.entities.PerfilEntity;
import com.rematec.voucher.voucherbackapi.utils.VoucherUtil;
import jakarta.transaction.Transactional;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
class PerfilServiceImpl implements IPerfilService {

    private final IPerfilRepository iPerfilRepository;
    private final VouckBackMapper mapper;
    private final VoucherUtil voucherUtil;
    private final IUsuarioRepository iUsuarioRepository;
    private final IEmpresaRepository iEmpresaRepository;

    public PerfilServiceImpl(final IPerfilRepository iPerfilRepository, final VouckBackMapper mapper,
                             final VoucherUtil voucherUtil, final IUsuarioRepository iUsuarioRepository,
                             final IEmpresaRepository iEmpresaRepository){

        this.iPerfilRepository = iPerfilRepository;
        this.mapper = mapper;
        this.voucherUtil = voucherUtil;
        this.iUsuarioRepository = iUsuarioRepository;
        this.iEmpresaRepository = iEmpresaRepository;
    }

    @Override
    public List<PerfilApiResponse> buscandoListaPerfil() {
        return this.mapper.listPerfilEntityToListPerfilApiResponse(this.iPerfilRepository.findAll());
    }

    @Override
    public List<PerfilApiResponse> buscandoListaPerfilPelaEmpresa(String guid) {
        return this.mapper.listPerfilEntityToListPerfilApiResponse(this.iPerfilRepository.findByEmpresaGuid(guid));
    }

    @Override
    public List<PerfilResumidoApiResponse> buscandoListaResumidoPerfil() {
        return this.mapper.listPerfilEntityToListPerfilResumidoApiResponse(this.iPerfilRepository.findAll());
    }

    @Override
    public PerfilApiResponse buscandoPerfilPeloGUID(String guid) {

        PerfilEntity entity = this.iPerfilRepository.findByGuid(guid)
                .orElseThrow(() -> new PerfilNaoEncontradoException("Perfil não encontrado."));

        return this.mapper.perfilEntityToPerfilApiResponse(entity);
    }

    @Override
    public PerfilApiResponse buscandoPerfilPeloNome(String nome) {

        PerfilEntity entity = this.iPerfilRepository.findByNome(nome)
                .orElseThrow(() -> new PerfilNaoEncontradoException("Perfil não encontrado."));

        return this.mapper.perfilEntityToPerfilApiResponse(entity);
    }

    @Override
    public PerfilApiResponse criandoPerfil(PerfilApiRequest perfilApiRequest) {


        if (!this.voucherUtil.checkDataNullAndEmpty(perfilApiRequest.getNome())) {
            throw new BadRequestException("Nome do perfil obrigatório.");
        }

        if (perfilApiRequest.getRoles() == null || perfilApiRequest.getRoles().isEmpty()) {
            throw new BadRequestException("Permissão do Perfil é Obrigatório.");
        }

        if (this.iPerfilRepository.findByNome(perfilApiRequest.getNome()).isPresent()) {
            throw new PerfilCadastradoException("Já existe um Perfil com este nome.");
        }

        if (!this.voucherUtil.checkDataNullAndEmpty(perfilApiRequest.getEmpresa())) {
            throw new BadRequestException("Identificacão da Empresa é Obrigatorio.");
        }

        EmpresaEntity empresaEntity = this.iEmpresaRepository.findByGuid(perfilApiRequest.getEmpresa())
                .orElseThrow(()-> new EmpresaNaoEncontradaException("Empresa não encontrada."));

        PerfilEntity perfilEntity = PerfilEntity.builder()
                .guid(UUID.randomUUID().toString())
                .nome(perfilApiRequest.getNome())
                .roles(this.voucherUtil.listRoleApiResponseToListRoleEntity(perfilApiRequest.getRoles()))
                .empresa(empresaEntity)
                .build();
        return this.mapper.perfilEntityToPerfilApiResponse(this.iPerfilRepository.save(perfilEntity));

    }

    @Override
    public PerfilApiResponse alterandoPerfil(String guid, PerfilUpdateApiRequest perfilApiRequest) {

        PerfilEntity entity = this.iPerfilRepository.findByGuid(guid)
                .orElseThrow(() -> new PerfilNaoEncontradoException("Perfil não encontrado."));

        if (this.voucherUtil.checkDataNullAndEmpty(perfilApiRequest.getNome())) {
            entity.setNome(perfilApiRequest.getNome());
        }

        if (perfilApiRequest.getRoles() != null && !perfilApiRequest.getRoles().isEmpty()) {
            entity.getRoles().clear();
            entity.getRoles().addAll(this.voucherUtil.listRoleApiResponseToListRoleEntity(perfilApiRequest.getRoles()));
        }

        return this.mapper.perfilEntityToPerfilApiResponse(this.iPerfilRepository.save(entity));

    }

    @Override
    public void apagandoPerfil(String guid) {

        PerfilEntity entity = this.iPerfilRepository.findByGuid(guid)
                .orElseThrow(() -> new PerfilNaoEncontradoException("Perfil não encontrado."));

        if (this.iUsuarioRepository.findTop1ByPerfisGuid(guid).isPresent()) {
            throw new NaoPermitidoExcluirPerfilException("Não permitido Excluir. Perfil associado a algum Usuario.");
        }
        this.iPerfilRepository.delete(entity);

    }

}
