package com.rematec.voucher.voucherbackapi.services;

import com.rematec.voucher.models.BuscandoListaPaginadaUsuario200Response;
import com.rematec.voucher.models.UpdateStatusApiRequest;
import com.rematec.voucher.models.UsuarioApiRequest;
import com.rematec.voucher.models.UsuarioApiResponse;
import com.rematec.voucher.models.UsuarioUpdateApiRequest;
import com.rematec.voucher.voucherbackapi.exceptios.BadRequestException;
import com.rematec.voucher.voucherbackapi.exceptios.EmpresaNaoEncontradaException;
import com.rematec.voucher.voucherbackapi.exceptios.UsuarioCadastradoException;
import com.rematec.voucher.voucherbackapi.exceptios.UsuarioNaoEncontradoException;
import com.rematec.voucher.voucherbackapi.mapper.VouckBackMapper;
import com.rematec.voucher.voucherbackapi.models.entities.EmpresaEntity;
import com.rematec.voucher.voucherbackapi.models.entities.UsuarioEntity;
import com.rematec.voucher.voucherbackapi.repositories.IEmpresaRepository;
import com.rematec.voucher.voucherbackapi.repositories.IUsuarioRepository;
import com.rematec.voucher.voucherbackapi.utils.VoucherUtil;
import jakarta.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service
@Transactional
class UsuarioServiceImpl implements IUsuarioService {

    private final IUsuarioRepository iUsuarioRepository;
    private final VouckBackMapper mapper;
    private final PasswordEncoder passwordEncoder;
    private final VoucherUtil voucherUtil;
    private final IEmpresaRepository iEmpresaRepository;

    public UsuarioServiceImpl(final IUsuarioRepository iUsuarioRepository, final VouckBackMapper mapper,
                              final PasswordEncoder passwordEncoder, final VoucherUtil voucherUtil,
                              final IEmpresaRepository iEmpresaRepository) {
        this.iUsuarioRepository = iUsuarioRepository;
        this.mapper = mapper;
        this.passwordEncoder = passwordEncoder;
        this.voucherUtil = voucherUtil;
        this.iEmpresaRepository = iEmpresaRepository;
    }

    @Override
    public List<UsuarioApiResponse> buscandoListaUsuario() {
        List<UsuarioEntity> list = this.iUsuarioRepository.findAll();

        System.out.println(list.stream().toList());

        return this.mapper.listUsuarioEntityTolistUsuarioApiResponse(this.iUsuarioRepository.findAll());
    }

    @Override
    public BuscandoListaPaginadaUsuario200Response buscandoListaPaginadaUsuario(String nome, Integer page, Integer size) {
        return this.mapper.pageUsuariosEntityToUsuariosApiPaginadaResponse(
                this.iUsuarioRepository.findByUserNameContaining(nome, PageRequest.of(page, size)));
    }

    @Override
    public UsuarioApiResponse buscandoUsuarioPeloGUID(String guid) {
        UsuarioEntity usuario = this.iUsuarioRepository.findByGuid(guid)
                .orElseThrow(() -> new UsuarioNaoEncontradoException("Usuario não encontrado."));
        return this.mapper.usuarioEntityToUsuarioApiResponse(usuario);
    }

    @Override
    public UsuarioApiResponse criandoUsuario(UsuarioApiRequest usuarioApiRequest) {

        if (!this.voucherUtil.checkDataNullAndEmpty(usuarioApiRequest.getEmail())) {
            throw new BadRequestException("E-mail do usuário obrigatório.");
        }

        if (!this.voucherUtil.checkDataNullAndEmpty(usuarioApiRequest.getPassword())) {
            throw new BadRequestException("Senha do usuário obrigatório.");
        }

        if (this.iUsuarioRepository.findByEmail(usuarioApiRequest.getEmail()).isPresent()) {
            throw new UsuarioCadastradoException("E-mail já cadastrado.");
        }
        if (usuarioApiRequest.getStatus() == null) {
            throw new BadRequestException("Status do usuário obrigatório.");
        }

        if (usuarioApiRequest.getPerfis() == null || usuarioApiRequest.getPerfis().isEmpty()) {
            throw new BadRequestException("Perfil do usuário obrigatório.");
        }

        if (!this.voucherUtil.checkDataNullAndEmpty(usuarioApiRequest.getUserName())) {
            throw new BadRequestException("Nome do usuário obrigatório.");
        }

        UsuarioEntity usuarioEntity = UsuarioEntity.builder()
                .guid(UUID.randomUUID().toString())
                .userName(usuarioApiRequest.getUserName())
                .email(usuarioApiRequest.getEmail())
                .perfis(this.voucherUtil.listUsuarioPerfilApiRequestToListPerfilEntity(usuarioApiRequest.getPerfis()))
                .status(usuarioApiRequest.getStatus())
                .lojas(this.voucherUtil.getListGuidApiRequestToListLojasEntity(usuarioApiRequest.getLojas()))
                .password(this.passwordEncoder.encode(usuarioApiRequest.getPassword()))
                .build();

        if (this.voucherUtil.checkDataNullAndEmpty(usuarioApiRequest.getEmpresa())) {

            EmpresaEntity empresaEntity = this.iEmpresaRepository.findByGuid(usuarioApiRequest.getEmpresa())
                    .orElseThrow(()-> new EmpresaNaoEncontradaException("Empresa não encontrada."));

            usuarioEntity.setEmpresa(empresaEntity);
        }

        return this.mapper.usuarioEntityToUsuarioApiResponse(this.iUsuarioRepository.save(usuarioEntity));
    }

    @Override
    public UsuarioApiResponse alterandoUsuario(String guid, UsuarioUpdateApiRequest usuarioUpdateApiRequest) {

        UsuarioEntity usuario = this.iUsuarioRepository.findByGuid(guid)
                .orElseThrow(() -> new UsuarioNaoEncontradoException("Usuario não encontrado."));

        if (this.voucherUtil.checkDataNullAndEmpty(usuarioUpdateApiRequest.getUserName()))
            usuario.setUserName(usuarioUpdateApiRequest.getUserName());

        if (this.voucherUtil.checkDataNullAndEmpty(usuarioUpdateApiRequest.getPassword()))
            usuario.setPassword(passwordEncoder.encode(usuarioUpdateApiRequest.getPassword()));

        if (this.voucherUtil.checkDataNullAndEmpty(usuarioUpdateApiRequest.getEmail())) {

            if (this.iUsuarioRepository.findByEmail(usuarioUpdateApiRequest.getEmail()).isPresent()) {
                if (!guid.equals(this.iUsuarioRepository.findByEmail(usuarioUpdateApiRequest.getEmail()).get().getGuid()))
                    throw new UsuarioCadastradoException("E-mail já cadastrado.");
            }
            usuario.setEmail(usuarioUpdateApiRequest.getEmail());
        }

        if (this.voucherUtil.checkDataNullAndEmpty(usuarioUpdateApiRequest.getEmpresa())) {

            EmpresaEntity empresaEntity = this.iEmpresaRepository.findByGuid(usuarioUpdateApiRequest.getEmpresa())
                    .orElseThrow(()-> new EmpresaNaoEncontradaException("Empresa não encontrada."));

            usuario.setEmpresa(empresaEntity);
        }

        if (usuarioUpdateApiRequest.getStatus() != null)
            usuario.setStatus(usuarioUpdateApiRequest.getStatus());

        if (usuarioUpdateApiRequest.getPerfis() != null && !usuarioUpdateApiRequest.getPerfis().isEmpty()) {
            usuario.setPerfis(
                    this.voucherUtil.listUsuarioPerfilApiRequestToListPerfilEntity(usuarioUpdateApiRequest.getPerfis())
            );
        }

        if (usuarioUpdateApiRequest.getLojas() != null && !usuarioUpdateApiRequest.getLojas().isEmpty()) {
            usuario.getLojas().clear();
            usuario.getLojas().addAll(
                    this.voucherUtil.getListGuidApiRequestToListLojasEntity(usuarioUpdateApiRequest.getLojas())
            );
        } else {
            usuario.setLojas(null);
        }

        return this.mapper.usuarioEntityToUsuarioApiResponse(this.iUsuarioRepository.save(usuario));

    }

    @Override
    public void apagandoUsuario(String guid) {
        UsuarioEntity usuario = this.iUsuarioRepository.findByGuid(guid)
                .orElseThrow(() -> new UsuarioNaoEncontradoException("Usuario não encontrado."));

        this.iUsuarioRepository.delete(usuario);
    }

    @Override
    public void alterandoStatusUsuario(String guid, UpdateStatusApiRequest updateStatusApiRequest) {
        UsuarioEntity usuario = this.iUsuarioRepository.findByGuid(guid)
                .orElseThrow(() -> new UsuarioNaoEncontradoException("Usuario não encontrado."));

        usuario.setStatus(updateStatusApiRequest.getStatus());

        this.mapper.usuarioEntityToUsuarioApiResponse(this.iUsuarioRepository.save(usuario));
    }
}
