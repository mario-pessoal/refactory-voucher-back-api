package com.rematec.voucher.voucherbackapi.services;

import com.rematec.voucher.models.EmpresaApiRequest;
import com.rematec.voucher.models.EmpresaApiResponse;
import com.rematec.voucher.models.EmpresaResumidoApiResponse;

import java.util.List;

public interface IEmpresaService {

    List<EmpresaApiResponse> buscandoListaEmpresa();
    EmpresaApiResponse criandoEmpresa(EmpresaApiRequest empresaApiRequest);
    EmpresaApiResponse buscandoEmpresaPeloGUID(String guid);
    EmpresaApiResponse alterandoEmpresa(String guid, EmpresaApiRequest empresaApiRequest);
    void apagandoEmpresa(String guid);
    List<EmpresaResumidoApiResponse> buscandoListaEmpresaResumido();
}
